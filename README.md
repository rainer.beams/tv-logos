# I'm no longer using github

Github decided to remove my account so that's why I've now set up this one. If you were using direct links then you need to update them:

### Example

Old URL: https://raw.githubusercontent.com/Tapiosinn/tv-logos/master/countries/united-kingdom/sky-sports-football-uk.png

New URL: https://gitlab.com/tapiosinn/tv-logos/-/raw/main/countries/united-kingdom/sky-sports-football-uk.png

So for all your links you need to replace https://raw.githubusercontent.com/Tapiosinn/tv-logos/master/ with https://gitlab.com/tapiosinn/tv-logos/-/raw/main/

You can use a "Find and replace all" function in a text editor.

# Free high quality TV Logos for dark backgrounds

Contact: tapio.sinnertwin(at)gmail.com - I will try my best to reply within 24hr.

Here you will find free tv logos from around the world in good high-res quality and suitable for dark backgrounds.

## 💛 Support me to continue this project

If you want to support me continuing the work, you can donate by clicking the donate button. Creating the logos and maintaining them and the github takes quite the time and I would like to put in more work on this project so any donation helps!

[<img src="https://gitlab.com/tapiosinn/tv-logos/-/raw/main/misc/paypal-donate.png" width="150">](https://www.paypal.me/9000hobbs)

## 📝 Logo request

[CLICK HERE](https://forms.gle/BVjAKFXwSCuWhpYi7) to go to a form request page where you can request a logo.

## 🌍 Countries

Click on a country to go to its directory. From there you can click the mosaic file at top to see the logos rendered.

|  ![Space]           |  ![Space]         |  ![Space]       |  ![Space]         |  ![Space]                  |  ![Space]              |
|--------------------|------------------|----------------|------------------|---------------------------|-----------------------|
| 🇦🇷 [Argentina]     | 🇦🇺 [Australia]   | 🇦🇹 [Austria]   | 🇧🇪 [Belgium]     | 🇨🇦 [Canada]               | 🌎 [Caribbean]         |
| 🇨🇷 [Costa-Rica]    | 🇭🇷 [Croatia]     | 🇩🇰 [Denmark]   | 🇫🇮 [Finland]     | 🇫🇷 [France]               | 🇩🇪 [Germany]          |
| 🇬🇷 [Greece]        | 🇭🇰 [Hong-Kong]   | 🇮🇸 [Iceland]   | 🇮🇳 [India]       | 🇮🇩 [Indonesia]            | 🌍 [International]     |
| 🇮🇹 [Italy]         | 🇱🇧 [Lebanon]     | 🇱🇹 [Lithuania] | 🇲🇾 [Malaysia]    | 🇲🇹 [Malta]                | 🇲🇽 [Mexico]           |
| 🇳🇱 [Netherlands]   | 🇳🇿 [New-Zealand] | 🌍 [Nordic]     | 🇳🇴 [Norway]      | 🇵🇱 [Poland]               | 🇵🇹 [Portugal]         |
| 🇷🇴 [Romania]       | 🇷🇺 [Russia]      | 🇷🇸 [Serbia]    | 🇸🇬 [Singapore]   | 🇿🇦 [South-Africa]         | 🇪🇸 [Spain]            |
| 🇸🇪 [Sweden]        | 🇨🇭 [Switzerland] | 🇹🇷 [Turkey]    | 🇺🇦 [Ukraine]     | 🇦🇪 [United-Arab-Emirates] | 🇬🇧 [United-Kingdom]   |
| 🇺🇸 [United-States] | 🌍 [World-Africa] | 🌏 [World-Asia] | 🌍 [World-Europe] | 🌎 [World-Latin-America]   | 🌏 [World-Middle-East] |
| 🌏 [World-Oceania]  | ⭐️ [Misc]         | 📼 [Vod]        |       |                |            |

[Argentina]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/argentina "Argentina"
[Australia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/australia "Australia"
[Austria]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/austria "Austria"
[Belgium]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/belgium "Belgium"
[Canada]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/canada "Canada"
[Caribbean]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/caribbean "Caribbean"
[Costa-Rica]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/costa-rica "Costa-Rica"
[Croatia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/croatia "Croatia"
[Denmark]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic/denmark "Denmark"
[Finland]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic/finland "Finland"
[France]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/france "France"
[Germany]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/germany "Germany"
[Greece]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/greece "Greece"
[Hong-Kong]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/hong-kong "Hong-Kong"
[Iceland]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic/iceland "Iceland"
[India]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/india "India"
[Indonesia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/indonesia "Indonesia"
[International]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/international "International"
[Italy]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/italy "Italy"
[Lebanon]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/lebanon "Lebanon"
[Lithuania]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/lithuania "Lithuania"
[Malaysia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/malaysia "Malaysia"
[Malta]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/malta "Malta"
[Mexico]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/mexico "Mexico"
[Netherlands]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/netherlands "Netherlands"
[New-Zealand]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/new-zealand "New-Zealand"
[Nordic]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic "Nordic"
[Norway]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic/norway "Norway"
[Poland]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/poland "Poland"
[Portugal]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/portugal "Portugal"
[Romania]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/romania "Romania"
[Russia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/russia "Russia"
[Serbia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/serbia "Serbia"
[Singapore]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/singapore "Singapore"
[South-Africa]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/south-africa "South-Africa"
[Spain]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/spain "Spain"
[Sweden]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/nordic/sweden "Sweden"
[Switzerland]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/switzerland "Switzerland"
[Turkey]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/turkey "Turkey"
[Ukraine]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/ukraine "Ukraine"
[United-Arab-Emirates]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/united-arab-emirates "United-Arab-Emirates"
[United-Kingdom]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/united-kingdom "United-Kingdom"
[United-States]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/united-states "United-States"
[World-Africa]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-africa "World-Africa"
[World-Asia]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-asia "World-Asia"
[World-Europe]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-europe "World-Europe"
[World-Latin-America]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-latin-america "World-Latin-America"
[World-Middle-East]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-middle-east "World-Middle-East"
[World-Oceania]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/countries/world-oceania "World-Oceania"
[Misc]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/miscmisc "Misc"
[Vod]:https://gitlab.com/tapiosinn/tv-logos/-/tree/main/misc/vodvod "Vod"

[Space]:https://gitlab.com/tapiosinn/tv-logos/-/raw/main/misc/space-1500.png "Space"

## About the project

The logos are adjusted to work on a dark background. Some of them are not suitable for light backgrounds. Since the Git interface is white you might think the logo is not loading but it is, just right-click on the logo and select copy image address and use that URL for the logo.

## I will regularly add new channel logos, you can also message me and request logos

If you want to support me continuing the work, you can donate by clicking the donate button: Donaters can jump in line and request missing logos for me to do as soon as I can.

[<img src="https://gitlab.com/tapiosinn/tv-logos/-/raw/main/misc/paypal-donate.png" width="150">](https://www.paypal.me/9000hobbs)

* Donators get a channel logo request. I also take requests for custom logos or logos for a light background.

* The goal of this project is to give people better access to logos in good quality and to meet the design requirements of the designers/owners of the logos and supply the increasing demand for better quality experience.

* All the logos come in PNG format and work perfectly on dark backgrounds.

* The logos are 512px in width. Please contact me if you need a specific logo in a specific resolution.

* I also provide logos for US local networks that can be hard to find.

* For a donation I can create a hard-to-find logo or skip the queue for a specific country/network of your own choice and provide the logo's in high quality, contact me for more info.

* The list is free for personal use. Please reference my work for any re-distribution. It's forbidden to sell the logos to 3rd party or use them in any illegitimate way. I do not take responsibility for what you do with the logos.

* I do not own the logos. All the logos in the project do not have copyright on them but instead they are trademarked and with Creative Commons ShareAlike licenses. PM me if you wish to remove/change a logo you own or submit your own logo.

* At some point in the future I plan to upload logos in SVG. This is not high priority right now.

* You can link directly to the RAW URL of any logo but I do not take any responsibility if links are broken. If you are having issues with the logos url's then contact me.

* If a channel changes its logo or rebrands, I will replace the original logo, if you are using direct link then the logo will update for you automatically. It can take some time for some devices/software to update the logo, you might need to restart/clear cache.

* If you would like to be able to link the logos to a service you provide, for example a website/service/app then please contact me first.

* Did you discover a mistake? Or is something not like it should be? Then please let me know by sending me a message/email at tapio.sinnertwin(at)gmail.com

## Naming convention

Filenames are all lowercase and start with channel name and then number if applicable and country code at the end. Dashes are used to replace spaces. Call signs for local networks are also lowercase and appear before country.

Examples: abc-7-koat-us.png | discovery-channel-us.png | syfy-br.png | fox-crime-it.png | kanal-5-se.png

Time delay channels are identified with "plus", example: challenge-plus-uk.png For Australia the +2 channels are fox-hits-plus-2-au.png

If ampersand(&) is present in channel name then it will be replaced with "and", example: a-and-e-us.png

For channels that dont authenticate with a network affiliate or Owned-and-operated station then I will use the call sign and/or city/region.

Example: 7-news-miami-wsvn-us.png | 7-news-boston-whdh-us.png

Be aware of the dash when searching for a logo in a long list of channels, include it in your search. You can also write your search string without the dashes and spaces, it might give better results

Example: channel-4 instead of channel 4, or just channel4

A horizontal version of a logo will have hz in its name if there's a regular version also. Example: bbc-world-news-hz-uk.png

This project is possible thanks to Creative Commons ShareAlike licenses, CC BY-SA and CC BY-NC-SA. By using the logos you have to agree with the following: The logo you are about to use is the intellectual property of the copyright, trademark holder and is offered to you as a convenience for lawful use.

For more info on the project or information please PM me or send me email at tapio.sinnertwin(at)gmail.com I will try my best to reply within 24hr. Thank you.
